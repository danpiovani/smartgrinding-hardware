update=Tue 12 Mar 2019 04:28:32 PM -03
version=1
last_client=pcbnew
[general]
version=1
[cvpcb]
version=1
NetIExt=net
[eeschema]
version=1
LibDir=../component_lib/symbols/
NetFmtName=PcbnewAdvanced
RptD_X=0
RptD_Y=100
RptLab=1
LabSize=60
[eeschema/libraries]
LibName1=austral
LibName2=power
LibName3=device
LibName4=transistors
LibName5=conn
LibName6=linear
LibName7=regul
LibName8=74xx
LibName9=cmos4000
LibName10=adc-dac
LibName11=memory
LibName12=xilinx
LibName13=microcontrollers
LibName14=dsp
LibName15=microchip
LibName16=analog_switches
LibName17=motorola
LibName18=texas
LibName19=intel
LibName20=audio
LibName21=interface
LibName22=digital-audio
LibName23=philips
LibName24=display
LibName25=cypress
LibName26=siliconi
LibName27=opto
LibName28=atmel
LibName29=contrib
LibName30=valves
LibName31=/home/garaujo/svn/kicad-symbols.git/trunk/Connector
[pcbnew]
version=1
LastNetListRead=austral_receiver_0v1.net
UseCmpFile=1
PadDrill=3.000000000000
PadDrillOvalY=3.000000000000
PadSizeH=3.000000000000
PadSizeV=3.000000000000
PcbTextSizeV=1.500000000000
PcbTextSizeH=1.500000000000
PcbTextThickness=0.300000000000
ModuleTextSizeV=1.000000000000
ModuleTextSizeH=1.000000000000
ModuleTextSizeThickness=0.150000000000
SolderMaskClearance=0.300000000000
SolderMaskMinWidth=0.300000000000
DrawSegmentWidth=0.200000000000
BoardOutlineThickness=0.100000000000
ModuleOutlineThickness=0.100000000000
[pcbnew/libraries]
LibDir=../components_lib/footprints.pretty
LibName1=BGA50C40P7X8_295X322X50
LibName2=Bosch_LGA-14_3x2.5mm_P0.5mm
LibName3=C_0201_0603Metric
LibName4=C_0402_1005Metric
LibName5=C_0603_1608Metric
LibName6=L_0201_0603Metric
LibName7=L_0402_1005Metric
LibName8=L_0603_1608Metric
LibName9=LGA-16_3x3mm_P0_5mm
LibName10=LGA-CC-16-4
LibName11=MM8130-2600
LibName12=nrf_prog
LibName13=PCONN
LibName14=QFN40P600X600X90-48N
LibName15=R_0201_0603Metric
LibName16=R_0402_1005Metric
LibName17=XTAL_2012
LibName18=XTAL_2016
LibName19=via
LibName20=PinHeader_2x25
LibName21=RF_SMA_Vertical
LibName22=SMA_Molex_73251-2200_Horizontal
LibName23=Fiducial
LibName24=Hole
