PCBNEW-LibModule-V1  Wed 27 Feb 2019 02:32:49 PM -03
# encoding utf-8
Units mm
$INDEX
L_0201_0603Metric
$EndINDEX
$MODULE L_0201_0603Metric
Po 0 0 0 15 5B301BBE 00000000 ~~
Li L_0201_0603Metric
Cd Inductor SMD 0201 (0603 Metric), square (rectangular) end terminal, IPC_7351 nominal, (Body size source: https://www.vishay.com/docs/20052/crcw0201e3.pdf), generated with kicad-footprint-generator
Kw inductor
Sc 0
AR 
Op 0 0 0
At SMD
T0 0 -1.05 1 1 0 0.15 N V 21 N "REF**"
T1 0 1.05 1 1 0 0.15 N V 27 N "L_0201_0603Metric"
DS -0.3 0.15 -0.3 -0.15 0.1 27
DS -0.3 -0.15 0.3 -0.15 0.1 27
DS 0.3 -0.15 0.3 0.15 0.1 27
DS 0.3 0.15 -0.3 0.15 0.1 27
DS -0.7 0.35 -0.7 -0.35 0.05 26
DS -0.7 -0.35 0.7 -0.35 0.05 26
DS 0.7 -0.35 0.7 0.35 0.05 26
DS 0.7 0.35 -0.7 0.35 0.05 26
T2 0 -0.68 0.25 0.25 0 0.04 N V 27 N "%R"
$PAD
Sh "" R 0.318 0.36 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po -0.345 0
.SolderPasteRatio 0.25
$EndPAD
$PAD
Sh "" R 0.318 0.36 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po 0.345 0
.SolderPasteRatio 0.25
$EndPAD
$PAD
Sh "1" R 0.46 0.4 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po -0.32 0
.SolderPasteRatio 0.25
$EndPAD
$PAD
Sh "2" R 0.46 0.4 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po 0.32 0
.SolderPasteRatio 0.25
$EndPAD
$SHAPE3D
Na "${KISYS3DMOD}/Inductor_SMD.3dshapes/L_0201_0603Metric.wrl"
Sc 1 1 1
Of 0 0 0
Ro 0 0 0
$EndSHAPE3D
$EndMODULE L_0201_0603Metric
$EndLIBRARY
