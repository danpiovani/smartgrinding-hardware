PCBNEW-LibModule-V1  Wed 27 Feb 2019 05:42:35 PM -03
# encoding utf-8
Units mm
$INDEX
L_0402_1005Metric
$EndINDEX
$MODULE L_0402_1005Metric
Po 0 0 0 15 5C76F632 00000000 ~~
Li L_0402_1005Metric
Cd Inductor SMD 0402 (1005 Metric), square (rectangular) end terminal, IPC_7351 nominal, (Body size source: http://www.tortai-tech.com/upload/download/2011102023233369053.pdf), generated with kicad-footprint-generator
Kw inductor
Sc 0
AR /5C6C2716
Op 0 0 0
At SMD
T0 -0.0254 -1.0795 0.6 0.6 0 0.1 N V 21 N "L1"
T1 0 1.17 1 1 0 0.15 N V 27 N "3.3nH"
DS -0.5 0.25 -0.5 -0.25 0.1 27
DS -0.5 -0.25 0.5 -0.25 0.1 27
DS 0.5 -0.25 0.5 0.25 0.1 27
DS 0.5 0.25 -0.5 0.25 0.1 27
DS -0.93 0.47 -0.93 -0.47 0.1 21
DS -0.93 -0.47 0.93 -0.47 0.1 21
DS 0.93 -0.47 0.93 0.47 0.1 21
DS 0.93 0.47 -0.93 0.47 0.1 21
T2 0 0 0.25 0.25 0 0.04 N V 27 N "%R"
$PAD
Sh "1" R 0.59 0.64 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 2 "N-0000018"
Po -0.485 0
.SolderPasteRatio 0.25
$EndPAD
$PAD
Sh "2" R 0.59 0.64 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 1 "N-0000010"
Po 0.485 0
.SolderPasteRatio 0.25
$EndPAD
$SHAPE3D
Na "${KISYS3DMOD}/Inductor_SMD.3dshapes/L_0402_1005Metric.wrl"
Sc 1 1 1
Of 0 0 0
Ro 0 0 0
$EndSHAPE3D
$EndMODULE L_0402_1005Metric
$EndLIBRARY
