PCBNEW-LibModule-V1  Tue 12 Mar 2019 04:06:28 PM -03
# encoding utf-8
Units mm
$INDEX
Fiducial
$EndINDEX
$MODULE Fiducial
Po 0 0 0 15 5C18CB26 00000000 ~~
Li Fiducial
Cd Circular Fiducial, 0.5mm bare copper, 1mm soldermask opening (Level C)
Kw fiducial
Sc 0
AR 
Op 0 0 0
At SMD
T0 0 -1.5 1 1 0 0.15 N V 21 N "REF**"
T1 0 1.5 1 1 0 0.15 N V 26 N "Fiducial"
DC 0 0 0.5 0 0.1 26
T2 0 0 0.2 0.2 0 0.04 N V 26 N "%R"
DC 0 0 0.75 0 0.05 27
$PAD
Sh "" C 0.5 0.5 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po 0 0
.SolderMask 0.25
.LocalClearance 0.25
$EndPAD
$EndMODULE Fiducial
$EndLIBRARY
