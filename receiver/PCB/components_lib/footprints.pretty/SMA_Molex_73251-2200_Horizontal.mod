PCBNEW-LibModule-V1  Tue 12 Mar 2019 10:21:24 AM -03
# encoding utf-8
Units mm
$INDEX
SMA_Molex_73251-2200_Horizontal
$EndINDEX
$MODULE SMA_Molex_73251-2200_Horizontal
Po 0 0 0 15 5C07CA7C 00000000 ~~
Li SMA_Molex_73251-2200_Horizontal
Cd https://www.molex.com/webdocs/datasheets/pdf/en-us/0732512200_RF_COAX_CONNECTORS.pdf
Kw SMA THT Female Jack Horizontal
Sc 0
AR 
Op 0 0 0
T0 -4.9 0 1 1 900 0.15 N V 21 N "REF**"
T1 0 5 1 1 0 0.15 N V 27 N "SMA_Molex_73251-2200_Horizontal"
DS 4.4 4.16 -4.4 4.16 0.05 26
DS 4.4 4.16 4.4 -17 0.05 26
DS -4.4 -17 -4.4 4.16 0.05 26
DS -4.4 -17 4.4 -17 0.05 26
DS -3.5 -3.95 3.5 -3.95 0.1 27
DS -3.5 -3.95 -3.5 3.05 0.1 27
DS -3.5 3.05 3.5 3.05 0.1 27
DS 3.5 -3.95 3.5 3.05 0.1 27
DS -3.6 -1.6 -3.6 1.6 0.12 21
DS 3.6 -1.6 3.6 1.6 0.12 21
DS -1.3 3.2 1.3 3.2 0.12 21
DS -1.8 -3.68 1.8 -3.68 0.12 21
T2 0 0 1 1 0 0.15 N V 27 N "%R"
DS -3 -16.5 3 -16.5 0.1 27
DS -3 -16.5 -3 -5.07 0.1 27
DS 3 -16.5 3 -5.07 0.1 27
DS 3.9 -3.95 3.9 -5.07 0.1 27
DS 3.9 -5.07 -3.9 -5.07 0.1 27
DS -3.9 -5.07 -3.9 -3.95 0.1 27
DS -3.9 -3.95 3.9 -3.95 0.1 27
DS -3 -6 3 -6.7 0.1 27
DS -3 -7 3 -7.7 0.1 27
DS -3 -8 3 -8.7 0.1 27
DS -3 -9 3 -9.7 0.1 27
DS -3 -10 3 -10.7 0.1 27
DS -3 -11 3 -11.7 0.1 27
DS -3 -12 3 -12.7 0.1 27
DS -3 -13 3 -13.7 0.1 27
DS -3 -14 3 -14.7 0.1 27
$PAD
Sh "1" C 2.25 2.25 0 0 0
Dr 1.5 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 0 0
$EndPAD
$PAD
Sh "2" C 2.25 2.25 0 0 900
Dr 1.5 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 2.54 2.54
$EndPAD
$PAD
Sh "2" C 2.25 2.25 0 0 0
Dr 1.5 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 2.54 -2.54
$EndPAD
$PAD
Sh "2" C 2.25 2.25 0 0 0
Dr 1.5 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -2.54 -2.54
$EndPAD
$PAD
Sh "2" C 2.25 2.25 0 0 0
Dr 1.5 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -2.54 2.54
$EndPAD
$SHAPE3D
Na "${KISYS3DMOD}/Connector_Coaxial.3dshapes/SMA_Molex_73251-2200_Horizontal.wrl"
Sc 1 1 1
Of 0 0 0
Ro 0 0 0
$EndSHAPE3D
$EndMODULE SMA_Molex_73251-2200_Horizontal
$EndLIBRARY
