PCBNEW-LibModule-V1  Wed 27 Feb 2019 05:52:54 PM -03
# encoding utf-8
Units mm
$INDEX
C_0201_0603Metric
$EndINDEX
$MODULE C_0201_0603Metric
Po 0 0 0 15 5C76F896 00000000 ~~
Li C_0201_0603Metric
Cd Capacitor SMD 0201 (0603 Metric), square (rectangular) end terminal, IPC_7351 nominal, (Body size source: https://www.vishay.com/docs/20052/crcw0201e3.pdf), generated with kicad-footprint-generator
Kw capacitor
Sc 0
AR /5C6B0F27
Op 0 0 0
At SMD
T0 0.00254 -0.83058 0.6 0.6 0 0.1 N V 21 N "C7"
T1 0 1.05 1 1 0 0.15 N V 27 N "100pF"
DS -0.3 0.15 -0.3 -0.15 0.1 27
DS -0.3 -0.15 0.3 -0.15 0.1 27
DS 0.3 -0.15 0.3 0.15 0.1 27
DS 0.3 0.15 -0.3 0.15 0.1 27
DS -0.7 0.35 -0.7 -0.35 0.1 21
DS -0.7 -0.35 0.7 -0.35 0.1 21
DS 0.7 -0.35 0.7 0.35 0.1 21
DS 0.7 0.35 -0.7 0.35 0.1 21
T2 -0.00762 0.02286 0.25 0.25 0 0.04 N V 27 N "%R"
$PAD
Sh "" R 0.318 0.36 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po -0.345 0
.SolderPasteRatio 0.25
$EndPAD
$PAD
Sh "" R 0.318 0.36 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po 0.345 0
.SolderPasteRatio 0.25
$EndPAD
$PAD
Sh "1" R 0.46 0.4 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 2 "N-000006"
Po -0.32 0
.SolderPasteRatio 0.25
$EndPAD
$PAD
Sh "2" R 0.46 0.4 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 1 "GND"
Po 0.32 0
.SolderPasteRatio 0.25
$EndPAD
$SHAPE3D
Na "${KISYS3DMOD}/Capacitor_SMD.3dshapes/C_0201_0603Metric.wrl"
Sc 1 1 1
Of 0 0 0
Ro 0 0 0
$EndSHAPE3D
$EndMODULE C_0201_0603Metric
$EndLIBRARY
