PCBNEW-LibModule-V1  Wed 27 Feb 2019 02:35:16 PM -03
# encoding utf-8
Units mm
$INDEX
L_0603_1608Metric
$EndINDEX
$MODULE L_0603_1608Metric
Po 0 0 0 15 5C76CA42 00000000 ~~
Li L_0603_1608Metric
Cd Inductor SMD 0603 (1608 Metric), square (rectangular) end terminal, IPC_7351 nominal, (Body size source: http://www.tortai-tech.com/upload/download/2011102023233369053.pdf), generated with kicad-footprint-generator
Kw inductor
Sc 0
AR 
Op 0 0 0
At SMD
T0 0 -1.43 1 1 0 0.15 N V 21 N "REF**"
T1 0 1.43 1 1 0 0.15 N V 27 N "L_0603_1608Metric"
DS -0.8 0.4 -0.8 -0.4 0.1 27
DS -0.8 -0.4 0.8 -0.4 0.1 27
DS 0.8 -0.4 0.8 0.4 0.1 27
DS 0.8 0.4 -0.8 0.4 0.1 27
DS -0.162779 -0.51 0.162779 -0.51 0.12 21
DS -0.162779 0.51 0.162779 0.51 0.12 21
DS -1.48 0.73 -1.48 -0.73 0.05 26
DS -1.48 -0.73 1.48 -0.73 0.05 26
DS 1.48 -0.73 1.48 0.73 0.05 26
DS 1.48 0.73 -1.48 0.73 0.05 26
T2 0 0 0.4 0.4 0 0.06 N V 27 N "%R"
$PAD
Sh "1" R 0.875 0.95 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -0.7875 0
.SolderPasteRatio 0.25
$EndPAD
$PAD
Sh "2" R 0.875 0.95 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0.7875 0
.SolderPasteRatio 0.25
$EndPAD
$SHAPE3D
Na "${KISYS3DMOD}/Inductor_SMD.3dshapes/L_0603_1608Metric.wrl"
Sc 1 1 1
Of 0 0 0
Ro 0 0 0
$EndSHAPE3D
$EndMODULE L_0603_1608Metric
$EndLIBRARY
