PCBNEW-LibModule-V1  Thu 07 Mar 2019 03:22:12 PM -03
# encoding utf-8
Units mm
$INDEX
VIA
$EndINDEX
$MODULE VIA
Po 0 0 0 15 5C81614C 00000000 ~~
Li VIA
Sc 0
AR 
Op 0 0 0
T0 1.27 -2.54 1 1 0 0.15 N V 21 N ""
T1 0 0 1 1 0 0.15 N V 21 N ""
$PAD
Sh "1" C 0.45 0.45 0 0 0
Dr 0.3 0 0
At STD N 0000FFFF
Ne 0 ""
Po 0 0
.LocalClearance 0.1
.ZoneConnection 0
$EndPAD
$EndMODULE VIA
$EndLIBRARY
