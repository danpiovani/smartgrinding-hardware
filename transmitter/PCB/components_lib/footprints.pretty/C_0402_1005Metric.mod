PCBNEW-LibModule-V1  Wed 27 Feb 2019 05:38:13 PM -03
# encoding utf-8
Units mm
$INDEX
C_0402_1005Metric
$EndINDEX
$MODULE C_0402_1005Metric
Po 0 0 0 15 5C76F51C 00000000 ~~
Li C_0402_1005Metric
Cd Capacitor SMD 0402 (1005 Metric), square (rectangular) end terminal, IPC_7351 nominal, (Body size source: http://www.tortai-tech.com/upload/download/2011102023233369053.pdf), generated with kicad-footprint-generator
Kw capacitor
Sc 0
AR /5C768D10
Op 0 0 0
At SMD
T0 0.00254 -0.9398 0.6 0.6 0 0.1 N V 21 N "C20"
T1 0 1.17 1 1 0 0.15 N V 27 N "1pF"
DS -0.5 0.25 -0.5 -0.25 0.1 27
DS -0.5 -0.25 0.5 -0.25 0.1 27
DS 0.5 -0.25 0.5 0.25 0.1 27
DS 0.5 0.25 -0.5 0.25 0.1 27
DS -0.93 0.47 -0.93 -0.47 0.1 21
DS -0.93 -0.47 0.93 -0.47 0.1 21
DS 0.93 -0.47 0.93 0.47 0.1 21
DS 0.93 0.47 -0.93 0.47 0.1 21
T2 0 0 0.25 0.25 0 0.04 N V 27 N "%R"
$PAD
Sh "1" R 0.59 0.64 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 2 "N-0000017"
Po -0.485 0
.SolderPasteRatio 0.25
$EndPAD
$PAD
Sh "2" R 0.59 0.64 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 1 "GND"
Po 0.485 0
.SolderPasteRatio 0.25
$EndPAD
$SHAPE3D
Na "${KISYS3DMOD}/Capacitor_SMD.3dshapes/C_0402_1005Metric.wrl"
Sc 1 1 1
Of 0 0 0
Ro 0 0 0
$EndSHAPE3D
$EndMODULE C_0402_1005Metric
$EndLIBRARY
